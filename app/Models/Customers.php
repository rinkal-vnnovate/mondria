<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    use HasFactory;

    public $table = 'customers';

    protected $fillable = [
       'id',
       'customer',
       'email',
       'propertyname',
       'contact',
       'block_id',
       'plan',
       'floor',
       'house_no',
       'sq_feet',
       'status',
       'created_at',
       'updated_at'
    ];

    public function block()
    {
        return $this->hasOne(Block::class, 'id', 'block_id');
    }

}

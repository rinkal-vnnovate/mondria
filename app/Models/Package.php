<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    public $table = 'packages';
    
    protected $fillable = [
       'id',
       'name',
       'status',
       'created_at',
       'updated_at'
    ];
}

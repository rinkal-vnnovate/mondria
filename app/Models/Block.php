<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    use HasFactory;
    public $table = 'blocks';
    
    protected $fillable = [
       'id',
       'name',
       'house_type',
       'created_at',
       'updated_at'
    ];
}

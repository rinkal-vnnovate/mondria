<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'role' => 'required',
            'name' => 'required',
            'email' => 'required',
//            'email' => "unique:users,email,$this->id,id",
            'password' => 'required|required_with:confirm_password|same:confirm_password|min:5',
        ];
    }
    public function messages(): array
    {
        return [
            'role.required' => 'Role field is Required!',
            'name.required' => 'Name field is Required!',
            'email.required' => 'Email field is Required!',
//            'email.unique' => 'Email Already Exist!',
            'password.required' => 'Password field is Required!',
            'password.required_with' => 'Please enter the same as password!',
        ];
    }
}

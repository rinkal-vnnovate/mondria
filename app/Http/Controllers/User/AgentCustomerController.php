<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RoleUser;
use App\Models\Customers;
use App\Models\Block;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

class AgentCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:AGENT');
    }
    public function index(Request $request)
    {
        // dd($data->user->name);
        $user = $request->user();
        if ($request->ajax()) {

            $data = Customers::with('block')->where('status','0')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('created_at', function ($request) {
                    return Carbon::parse($request->created_at)->toDateString();
                })
                ->editColumn('block_id', function ($request) {
                    return $request->block->name;
                })
                ->addColumn('action', function($row){
                    $button ='<a href="'. route('user.customer.show',$row->id).'"> <i class="fas fa-eye"></i></a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('user.customers.customer_info',compact('user'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Request $request,$id)
    {
        $data = Customers::with('block')->where('id',$id)->first();
        $user = $request->user();
        return view('user.customers.customer_view',compact('data','user'));
    }

    public function edit(Request $request,$id)
    {
      //
    }

    public function update(Request $request)
    {
       //
    }

    public function destroy($id)
    {
        // dd($id);
        $delete = Customers::where('id',$id)->update(['status' => 1]);
        return response()->json(['success' => true]);
    }
}

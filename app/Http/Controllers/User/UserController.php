<?php

namespace App\Http\Controllers\User;

use App\Models\Customers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:AGENT');
    }
    //
    public function index(Request $request)
    {
        $user = $request->user();
        $agent = User::where('role','AGENT')->get();
        $customer = Customers::where('status',0)->get();
//        dd($customer);
        return view('user.home',compact('user','customer','agent'));
    }
    public function profile(Request $request)
    {
        $user = $request->user();
        return view('user.profile',compact('user'));
    }
    public function updateProfile(Request $request)
    {
//        dd('222');
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required | email',
            'avatar' => 'image|mimes:jpg,jpeg,png,gif'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', $validator->messages()->first());
        }
        $user = $request->user();
        if (isset($request->email)) {
            $check = User::where('email', $request->email)
                ->where('id', '!=', $user->id)
                ->first();

            if ($check) {
                return response([
                    'message' => 'The email address is already used!',
                    'success' => 0,
                ]);
            }
        }
        $fileName = null;
        if (request()->hasFile('avatar')) {
            $file = request()->file('avatar');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('./uploads/', $fileName);
        }
        $data = [
            'id' => $request->id,
            'name' => $request->name,
            'email' => $request->email,
            'avatar' => $fileName,
        ];
//        dd($data);
        $user = User::find($request->id);
        $user = $user->update($data);

        return redirect('/user/profile')->with('success', 'Profile Updated Successfully');
    }
    public function updatePassword(Request $request)
    {
//        dd($request->all());
        $validator = Validator::make($request->all(), [
            'password' => 'required_with:confirm_password|same:confirm_password|min:6',
        ]);

        if ($validator->fails()) {
            dd($validator->messages()->first());
            return redirect()->back()->withInput()->with('error', $validator->messages()->first());
        }

        $data = [
            'id' => $request->id,
            'password' => Hash::make($request->password),
            'confirm_password' => Hash::make($request->confirm_password),
        ];
//        dd($data);
        $user = User::find($request->id);
        $user = $user->update($data);
        return redirect('/user/profile')->with('success', 'Password Updated Successfully');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\StoreUserRequest;
use App\Models\Customers;
use App\Models\User;
use App\Models\RoleUser;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use DB;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */

    /**
     * Login user and create token
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $email=$request->email;
        $password=$request->password;

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            $user = $request->user();
            $agent = User::where('role','AGENT')->get();
            $customer = Customers::where('status',0)->get();
            if($user->role == 'ADMIN'){
                return view('admin/home',compact('user','customer','agent'));
            }
            elseif($user->role == 'AGENT'){
                return view('user/home',compact('user','customer','agent'));
            }else{
                return view('auth/login');
            }

        }
        else
        {
            return redirect('/login')->with('error', 'Email Or Password would be Wrong!!!');
        }
    }
    public function ForgetPassword() {
        return view('auth.forget_password');
    }
    public function ForgetPasswordStore(Request $request) {
//        dd('5555');
        $request->validate([
            'email' => 'required|email|exists:users',
        ]);

        $token = Str::random(64);
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        Mail::send('auth.forget-password-email', ['token' => $token], function($message) use($request){
            $message->to($request->email);
            $message->from(env('MAIL_FROM_ADDRESS','monderia@gmail.com'), env('APP_NAME','Monderia'));
            $message->subject('Reset Password');
//            dd($message);
        });

        return back()->with('success', 'We have emailed your password reset link!');
    }
    public function ResetPassword($token) {
        return view('auth.forget-password-link', ['token' => $token]);
    }
    public function ResetPasswordStore(Request $request) {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        $update = DB::table('password_resets')->where(['email' => $request->email, 'token' => $request->token])->first();

        if(!$update){
            return back()->withInput()->with('error', 'Invalid token!');
        }

        $user = User::where('email', $request->email)->update(['password' => Hash::make($request->password)]);

        // Delete password_resets record
        DB::table('password_resets')->where(['email'=> $request->email])->delete();

        return redirect('/login')->with('success', 'Your password has been successfully changed!');
    }

    /**
     * Get the authenticated User
     *
     * @param Request $request
     * @return JsonResponse [json] user object
     */
    public function user(Request $request): JsonResponse
    {
        return response()->json($request->user());
    }
    public function register(Request $request)
    {
//        dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required_with:confirm_password|same:confirm_password|min:5',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', $validator->messages()->first());
        }
        $user = $request->user();
        if (isset($request->email)) {
            $check = User::where('email', $request->email)->first();

            if($check){
                return redirect('/register')->with('error', 'Email Already Exist!!!');
            }

        }
        $data = [
            'role' => "AGENT",
            'name' => $request->name,
            'email' => $request->email,
            'status' => 1,
            'password' => Hash::make($request->password),
            'mobile' => $request->mobile,
            'created_at' =>  Carbon::parse($request->created_at)->toDateString(),
        ];
//        dd($data);
        $user = new User($data);
        $user->save();
        if($user){
            $RoleUser = [
                'user_id' => $user->id,
                'role_id' => 2,
            ];
            $roleUser = new RoleUser($RoleUser);
            $roleUser->save();
            if($roleUser){
                return redirect('/login')->with('success', 'You are Register Successfully');
            }else{
                return redirect('/register')->with('error', 'Failed to create Register! Try again.');
            }
        }else{
            return redirect('/register')->with('error', 'Failed to create Register! Try again.');
        }
    }
    public function emailExist(Request $request)
    {
        if($request->email !== ''){
            $rule = array('email' => 'Required|email|unique:users');
            $validator = Validator::make($request->all(), $rule);
        }
        if (!$validator->fails()) {
//            die('true');
            return redirect('/users/create')->with('error', 'Email Already Exist!!!');
        }
    }


    /**
     * Logout user (Revoke the token)
     *
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/login');
//        return view('auth/login');
//        $this->user()->logout();

//        $request->session()->invalidate();
//
//        $request->session()->regenerateToken();

//        if ($response = $this->loggedOut($request)) {
//            return $response;
//        }

//        return $request->wantsJson()
//            ? new Response('', 204)
//            : redirect('/auth.login');
//        return redirect()->route('login');
    }

//        return redirect()->route('auth.login',$user);


}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\RoleUser;
use App\Models\User;
use App\Http\Requests\Users\StoreUserRequest;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Hash;

class UserManagementController extends Controller
{
    //
    public function __construct()

    {
        $this->middleware('auth');
        $this->middleware('role:ADMIN');
    }
    public function index(Request $request)
    {
        $user = $request->user();
        if ($request->ajax()) {

            $data = User::query()->where('status',1)->where('role','AGENT')->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('status', function ($request) {
                    if($request->status == 1){
                        return "<lable style='color: #0e4cfd;'>Active</lable>";
                    }
                })
                ->editColumn('created_at', function ($request) {
                    return Carbon::parse($request->created_at)->toDateString();
                })
                ->addColumn('action', function($row){
                    $button ='<a href="'.url('/admin/users/view/'.$row->id).'"> <i class="fas fa-eye"></i></a><a href="'.url('/admin/users/edit/'.$row->id).'"> <i class="fas fa-edit"></i></a>
                              <a type="submit" class="deleteUser" data-id='.$row->id.' data-token="'.csrf_token().'" ><i class="fas fa-trash-can fa-fw" style="color:red;"></i></a>';
//
                    return $button;
                })
                ->rawColumns(['action','status'])
                ->make(true);
        }
        return view('admin.users.user',compact('user'));
    }

    public function create(Request $request)
    {
        $user = $request->user();
        return view('admin.users.create',compact('user'));
    }
    public function store(StoreUserRequest $request)
    {
        if (isset($request->email)) {
            $check = User::where('email', $request->email)->first();

            if($check){
                return redirect('/admin/users/create')->with('error', 'Email Already Exist!!!');
            }

        }
        $data = [
            'role' => $request->role,
            'name' => $request->name,
            'email' => $request->email,
            'status' => 1,
            'password' => Hash::make($request->password),
            'created_at' =>  Carbon::parse($request->created_at)->toDateString(),
        ];

//        dd($data);
        $user = new User($data);
        $user->save();

        if($user){
            if($user->role == "ADMIN"){
                $RoleUser = [
                    'user_id' => $user->id,
                    'role_id' => 1,
                ];
            }elseif($user->role == "AGENT"){
                $RoleUser = [
                    'user_id' => $user->id,
                    'role_id' => 2,
                ];
            }

            $roleUser = new RoleUser($RoleUser);
            $roleUser->save();
            if($roleUser){
                return redirect('/admin/users')->with('success', 'New User Successfully Created');
            }else{
                return redirect('/admin/users')->with('error', 'Failed to create new user! Try again.');
            }
        }else{
            return redirect('/admin/users')->with('error', 'Failed to create new user! Try again.');
        }

    }
    public function edit($id,Request $request)
    {
        $data = User::where('id',"=", $id)->first();
        $user = $request->user();
        return view('admin.users.edit',compact('data','user'));
    }
    public function view($id,Request $request)
    {
        $data = User::where('id',"=", $id)->first();
        $user = $request->user();
        return view('admin.users.view',compact('data','user'));
    }
    public function update(StoreUserRequest $request)
    {
//    dd($request->all());
        $data = [
            'id' => $request->id,
            'role' => $request->role,
            'name' => $request->name,
            'email' => $request->email,
            'status' => 1,
            'password' => Hash::make($request->password),
            'updated_at' =>  Carbon::parse($request->updated_at)->toDateString(),
        ];
//        dd($data);
        $user = User::where('id',$request->id)->update($data);

        if($user){
            return redirect('/admin/users')->with('success', 'User Updated.');
        }else{
            return redirect('/admin/users')->with('error', 'Failed to Update user! Try again.');
        }

    }
    public function destroy($id)
    {
        $delete = User::where('id',$id)->update(['status' => 0]);
        return response()->json(['success' => true]);
    }
}

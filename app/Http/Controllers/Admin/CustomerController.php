<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RoleUser;
use App\Models\Customers;
use App\Models\Block;
use Carbon\Carbon;
use DataTables;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:ADMIN');
    }
    public function index(Request $request)
    {
        // dd($data->user->name);
        $user = $request->user();
        if ($request->ajax()) {

            $data = Customers::with('block')->where('status','0')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('created_at', function ($request) {
                    return Carbon::parse($request->created_at)->toDateString();
                })
                ->editColumn('block_id', function ($request) {
                    return $request->block->name;
                })
                ->editColumn('resendlink', function ($row) {
                     $button = '<a type="submit" id="forgetPassword" class = "btn btn-link"  data-email='.$row->email.' style="color: #0e4cfd;" >Reset Password</a>';
                    return $button;
                })
                ->addColumn('action', function($row){
                    $button ='<a  class = "" href="'. route('admin.customer.show',$row->id).'" > <i class="fas fa-eye" ></i></a>';
//                    <a type="submit" class="deleteUser" data-id='.$row->id.' data-token="'.csrf_token().'" ><i class="fas fa-trash-can fa-fw" style="color:red;"></i></a>
                    return $button;
                })

                ->rawColumns(['action','resendlink'])
                ->make(true);
        }

        return view('admin.customers.customer_info',compact('user'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Request $request,$id)
    {
        $data = Customers::with('block')->where('id',$id)->first();
        $user = $request->user();
        return view('admin.customers.customer_view',compact('data','user'));
    }

    public function edit(Request $request,$id)
    {
      //
    }

    public function update(Request $request)
    {
       //
    }

    public function destroy($id)
    {
        // dd($id);
        $delete = Customers::where('id',$id)->update(['status' => 1]);
        return response()->json(['success' => true]);
    }
}

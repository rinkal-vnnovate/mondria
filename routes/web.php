<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\UserManagementController;
use App\Http\Controllers\Admin\PackageController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\User\AgentCustomerController;
use App\Http\Controllers\User\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});
Route::get('/register', function () {
    return view('auth/register');
});
Route::get('/forget_password', function () {
    return view('auth/forget_password');
});
Route::post('/register', [AuthController::class, 'register']);
Route::post('/email_exist', [AuthController::class, 'emailExist'])->name('emailExist');
Route::get('forget_password', [AuthController::class, 'ForgetPassword'])->name('ForgetPasswordGet');
Route::post('ForgetPasswordPost', [AuthController::class, 'ForgetPasswordStore'])->name('ForgetPasswordPost');
Route::get('reset-password/{token}', [AuthController::class, 'ResetPassword'])->name('ResetPasswordGet');
Route::post('reset-password', [AuthController::class, 'ResetPasswordStore'])->name('ResetPasswordPost');

Route::group(['middleware' => 'auth'], function () {
    //start Admin routes
    Route::group(
        ['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['role:ADMIN']], function () {
        Route::get('/home', [AdminController::class, 'index'])->name('admin.home');
        Route::get('/profile', [AdminController::class, 'profile'])->name('admin.profile');
        Route::post('/updateProfile', [AdminController::class, 'updateProfile'])->name('admin.updateProfile');
        Route::post('/updatePassword', [AdminController::class, 'updatePassword'])->name('admin.updatePassword');

        //User Management
        Route::get('/users', [UserManagementController::class, 'index'])->name('users.index');
        Route::get('users/create',  [UserManagementController::class, 'create'])->name('users.create');
        Route::post('users/store',  [UserManagementController::class, 'store'])->name('users.store');
        Route::post('users/update',  [UserManagementController::class, 'update'])->name('users.update');
        Route::get('users/edit/{id}',  [UserManagementController::class, 'edit'])->name('users.edit');
        Route::get('users/view/{id}',  [UserManagementController::class, 'view'])->name('users.view');
        Route::post('users/delete/{id}',  [UserManagementController::class, 'destroy'])->name('users.destroy');

        //customer info Management
        Route::get('/customer_info', [CustomerController::class, 'index'])->name('customer.index');
        Route::get('/customer_delete/{id}', [CustomerController::class, 'destroy'])->name('customer.destroy');
        Route::get('/customer_view/{id}',  [CustomerController::class, 'show'])->name('customer.show');

        //Package Management
//        Route::get('/packages', [PackageController::class, 'index'])->name('packages.index');
//       Route::get('packages/create',  [PackageController::class, 'create'])->name('packages.create');
//       Route::post('packages/store',  [PackageController::class, 'store'])->name('packages.store');
//       Route::post('packages/update',  [PackageController::class, 'update'])->name('packages.update');
//       Route::get('packages/edit/{id}',  [PackageController::class, 'edit'])->name('packages.edit');
//       Route::get('packages/view/{id}',  [PackageController::class, 'view'])->name('packages.view');
//       Route::post('packages/delete/{id}',  [PackageController::class, 'destroy'])->name('packages.destroy');

    });

    //start Users routes
    Route::group(
        ['namespace' => 'User', 'prefix' => 'user', 'as' => 'user.', 'middleware' => ['role:AGENT']], function () {
        Route::get('/home', [UserController::class, 'index'])->name('user.home');
        Route::get('/profile', [UserController::class, 'profile'])->name('user.profile');
        Route::post('/updateProfile', [UserController::class, 'updateProfile'])->name('user.updateProfile');
        Route::post('/updatePassword', [UserController::class, 'updatePassword'])->name('user.updatePassword');

        //customer info Management
        Route::get('/customer_info', [AgentCustomerController::class, 'index'])->name('customer.index');
        Route::get('/customer_delete/{id}', [AgentCustomerController::class, 'destroy'])->name('customer.destroy');
        Route::get('/customer_view/{id}',  [AgentCustomerController::class, 'show'])->name('customer.show');
    });
});


Route::get('/logout', [AuthController::class, 'logout']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/login', function () {
    return view('auth/login');
});
\URL::forceScheme('https');




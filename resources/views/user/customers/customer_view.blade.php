@extends('includes.nav')
@include('includes.header')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Customer-View</title>
   <!-- core:css -->
   <link rel="stylesheet" href="../../../assets/vendors/core/core.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="../../../assets/vendors/select2/select2.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/jquery-tags-input/jquery.tagsinput.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/dropzone/dropzone.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/dropify/dist/dropify.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.min.css">
    <!-- end plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="../../../assets/fonts/feather-font/css/iconfont.css">
    <link rel="stylesheet" href="../../../assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="../../../assets/css/demo_1/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="../../../assets/images/favicon.png" />
</head>
<body>
<div class="main-wrapper">
    <div class="page-wrapper">
        <div class="page-content">
            <nav class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('/admin/customer_info'); ?>">Customers</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Customer Details</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title">Customer Details</h6>
                            <div id="accordion" class="accordion" role="tablist">
                                <div class="card">
                                   <div class="card-body table-responsive">
                                       <div class="form-row">
                                           <div class="col-md-4 mb-3">
                                               <div class="form-group">
                                                    <div class="row">
                                                       <strong>Customer Name: </strong>
                                                        <p class="px-2">{{ $data->customer}}</p>
                                                    </div>
                                               </div>
                                           </div>
                                           <div class="col-md-4 mb-3">
                                               <div class="form-group">
                                                   <div class="row">
                                                       <strong>Contact No.: </strong>
                                                       <p class="px-2">{{ $data->contact }}</p>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>

                                            <div class="form-row">
                                               <div class="col-md-4 mb-3">
                                                   <div class="form-group">
                                                       <div class="row">
                                                           <strong>Email: </strong>
                                                           <p class="px-2">{{ $data->email }}</p>
                                                       </div>
                                                   </div>
                                               </div>
                                                <div class="col-md-4 mb-3">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <strong>Block: </strong>
                                                            <p class="px-2">{{ $data['block']->name }}</p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                           <div class="form-row">

                                               <div class="col-md-4 mb-3">
                                                   <div class="form-group">
                                                       <div class="row">
                                                           <strong>Plan: </strong>
                                                           <p class="px-2">{{ $data->plan }}</p>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-md-4 mb-3">
                                                   <div class="form-group">
                                                       <div class="row">
                                                           <strong>Floor: </strong>
                                                           <p class="px-2">{{$data->floor }}</p>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                            <div class="form-row">
                                                <div class="col-md-4 mb-3">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <strong>House No.: </strong>
                                                            <p class="px-2" style="word-wrap: normal;">{{ $data->house_no }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 mb-3">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <strong>Square Feet: </strong>
                                                            <p class="px-2">{{$data->sq_feet}} ft</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-row">

                                                <div class="col-md-4 mb-3">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <strong>Created Date: </strong>
                                                            <p class="px-2" style="word-wrap: normal;">{{ date('d-m-Y', strtotime($data['created_at'])) }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <!-- partial:../../partials/_footer.html -->
        <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
            <p class="text-muted text-center text-md-left">Copyright © 2022 <a href="https://www.nobleui.com" target="_blank">Mondria</a>. All rights reserved</p>
            <p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Handcrafted With <i class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p>
        </footer>
        <!-- partial -->

    </div>
</div>

<!-- core:js -->
<script src="../../../assets/vendors/core/core.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<script src="../../../assets/vendors/jquery-validation/jquery.validate.min.js"></script>
<script src="../../../assets/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="../../../assets/vendors/inputmask/jquery.inputmask.min.js"></script>
<script src="../../../assets/vendors/select2/select2.min.js"></script>
<script src="../../../assets/vendors/typeahead.js/typeahead.bundle.min.js"></script>
<script src="../../../assets/vendors/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="../../../assets/vendors/dropzone/dropzone.min.js"></script>
<script src="../../../assets/vendors/dropify/dist/dropify.min.js"></script>
<script src="../../../assets/vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="../../../assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="../../../assets/vendors/moment/moment.min.js"></script>
<script src="../../../assets/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.js"></script>
<!-- end plugin js for this page -->
<!-- inject:js -->
<script src="../../../assets/vendors/feather-icons/feather.min.js"></script>
<script src="../../../assets/js/template.js"></script>
<!-- endinject -->
<!-- custom js for this page -->
<script src="../../../assets/js/form-validation.js"></script>
<script src="../../../assets/js/bootstrap-maxlength.js"></script>
<script src="../../../assets/js/inputmask.js"></script>
<script src="../../../assets/js/select2.js"></script>
<script src="../../../assets/js/typeahead.js"></script>
<script src="../../../assets/js/tags-input.js"></script>
<script src="../../../assets/js/dropzone.js"></script>
<script src="../../../assets/js/dropify.js"></script>
<script src="../../../assets/js/bootstrap-colorpicker.js"></script>
<script src="../../../assets/js/datepicker.js"></script>
<script src="../../../assets/js/timepicker.js"></script>
<!-- end custom js for this page -->
</body>
</html>

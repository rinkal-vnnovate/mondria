<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Monderia</title>
    <!-- core:css -->
    <link rel="stylesheet" href="../assets/vendors/core/core.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="../assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
    <!-- end plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="../assets/fonts/feather-font/css/iconfont.css">
    <link rel="stylesheet" href="../assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="../../../assets/vendors/select2/select2.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/jquery-tags-input/jquery.tagsinput.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/dropzone/dropzone.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/dropify/dist/dropify.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.min.css">
    <!-- end plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="../../../assets/css/demo_1/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="../../../assets/images/favicon.png" />
    <!-- endinject -->
    <!-- Layout styles -->
    <!-- End layout styles -->
</head>
<div class="page-wrapper">
<nav class="navbar">
    <a href="#" class="sidebar-toggler">
        <i data-feather="menu"></i>
    </a>
    <div class="navbar-content" style="margin-top: 5px;">

        <ul class="navbar-nav">

            <li class="nav-item dropdown nav-profile">
                <a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php
                        if($user['avatar'] == ''){ ?>
                            <img src="<?php echo URL::to('/uploads/avatar.jpg'); ?>" alt="profile" />
                        <?php
                        }else{
                        ?>
                            <img src="<?php echo URL::to('/uploads'); ?>/{{ $user->avatar }}" alt="profile" />
                        <?php
                        }
                    ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="profileDropdown">
                    <div class="dropdown-header d-flex flex-column align-items-center">
                        <div class="figure mb-3">
                            <?php
                                if($user['avatar'] == ''){ ?>
                                    <img src="<?php echo URL::to('/uploads/avatar.jpg'); ?>" alt="" />
                                <?php
                                }else{
                                ?>
                                    <img src="<?php echo URL::to('/uploads'); ?>/{{ $user->avatar }}" alt="" />
                                <?php
                                }
                            ?>
                        </div>
                        <div class="info text-center">
                            <p class="name font-weight-bold mb-0">{{ $user->name }}</p>
                            <p class="email text-muted mb-3">{{ $user->email }}</p>
                        </div>
                    </div>
                    <div class="dropdown-body">
                        <ul class="profile-nav p-0 pt-3">
                           <?php
                            if($user['role'] == 'ADMIN'){
                                ?>
                                <li class="nav-item">
                                <a href="<?php echo URL::to('/admin/profile'); ?>" class="nav-link">
                                    <i data-feather="user"></i>
                                    <span>Profile</span>
                                </a>
                            </li>
                            <?php }else if($user['role'] == 'AGENT'){
                                ?>
                                   <li class="nav-item">
                                <a href="<?php echo URL::to('/user/profile'); ?>" class="nav-link">
                                    <i data-feather="user"></i>
                                    <span>Profile</span>
                                </a>
                            </li>

                           <?php    } ?>


                            <li class="nav-item">
                                <a href="<?php echo URL::to('/logout'); ?>" class="nav-link">
                                    <i data-feather="log-out"></i>
                                    <span>Log Out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>
</div>

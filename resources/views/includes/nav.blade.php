<nav class="sidebar" >
    <div class="sidebar-header">
        <a href="#" class="sidebar-brand">
            Monderia
            {{--            <span>UI</span>--}}
        </a>
        <div class="sidebar-toggler not-active">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="sidebar-body">
        <ul class="nav">
            <li class="nav-item nav-category">Main</li>

            <li class="nav-item {{  request()->routeIs('admin.home') ? 'active' : '' }} ">
                <a href="<?php echo URL::to('/admin/home'); ?>" class="nav-link">
                    <i class="link-icon" data-feather="box"></i>
                    <span class="link-title">Dashboard</span>
                </a>
            </li>

{{--            <?php--}}
{{--            if($user['role'] == 'ADMIN'){--}}
{{--            ?>--}}
{{--            <li class="nav-item">--}}
{{--                <a href="<?php echo URL::to('/admin/users'); ?>" class="nav-link">--}}
{{--                    <i class="link-icon" data-feather="users"></i>--}}
{{--                    <span class="link-title">Agent Management</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <?php }else if($user['role'] == 'AGENT'){--}}
{{--            } --}}
{{--            ?>--}}

            <li class="nav-item {{  request()->routeIs('admin.customer_info') ? 'active' : '' }}{{  request()->routeIs('admin.customer.show') ? 'active' : '' }}">
                <a href="<?php echo URL::to('/admin/customer_info'); ?>" class="nav-link">
                    <i class="link-icon" data-feather="users"></i>
                    <span class="link-title">Customers</span>
                </a>
            </li>


{{--            <li class="nav-item">--}}
{{--                <a href="<?php echo URL::to('/admin/packages'); ?>" class="nav-link">--}}
{{--                    <i class="link-icon" data-feather="box"></i>--}}
{{--                    <span class="link-title">Packages</span>--}}
{{--                </a>--}}
{{--            </li>--}}

{{--            <li class="nav-item nav-category">web apps</li>--}}
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link" data-toggle="collapse" href="#emails" role="button" aria-expanded="false" aria-controls="emails">--}}
{{--                    <i class="link-icon" data-feather="mail"></i>--}}
{{--                    <span class="link-title">Email</span>--}}
{{--                    <i class="link-arrow" data-feather="chevron-down"></i>--}}
{{--                </a>--}}
{{--                <div class="collapse" id="emails">--}}
{{--                    <ul class="nav sub-menu">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="#" class="nav-link">Inbox</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="#" class="nav-link">Read</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="#" class="nav-link">Compose</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </li>--}}

        </ul>
    </div>
</nav>

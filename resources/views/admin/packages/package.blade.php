@extends('includes.nav')
@include('includes.header')
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin | Package</title>
    <!-- core:css -->
    <link rel="stylesheet" href="../../../assets/vendors/core/core.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="../../../assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
    <!-- end plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="../../../assets/fonts/feather-font/css/iconfont.css">
    <link rel="stylesheet" href="../../../assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">

    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="../../../assets/css/demo_1/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="../../../assets/images/favicon.png"/>
    <!-- core:js -->
    <script src="../../../assets/vendors/core/core.js"></script>
    <!-- endinject -->
    <!-- plugin js for this page -->
    <script src="../../../assets/vendors/datatables.net/jquery.dataTables.js"></script>
    <script src="../../../assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
    <!-- end plugin js for this page -->
    <!-- inject:js -->
    <script src="../../../assets/vendors/feather-icons/feather.min.js"></script>
    <script src="../../../assets/js/template.js"></script>
</head>
<body>
<div class="main-wrapper">
    <div class="page-wrapper">
        <div class="page-content">
            <nav class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Packages Management</li>
                </ol>
            </nav>
            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="ik ik-x"></i>
                    </button>
                </div>
            @endif

            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        {{-- table   --}}
                        <div class="card-body">
                            <h6 class="card-title">Packages</h6>

                            <a class="btn btn-primary m-1" href="<?php echo URL::to('/admin/packages/create'); ?>">Add
                                Package</a>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>Action</th>
                                        <th>Plan</th>
                                        <th>Square-feet</th>
                                        <th>Package Name</th>
                                        <th>Sub-Package</th>
                                        <th>Image</th>
                                        <th>Total Cost</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- partial:../../partials/_footer.html -->
        <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
            <p class="text-muted text-center text-md-left">Copyright © 2020 <a href="https://www.nobleui.com"
                                                                               target="_blank">NobleUI</a>. All rights
                reserved</p>
            <p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Handcrafted With <i
                    class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p>
        </footer>
        <!-- partial -->
    </div>
</div>
</body>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".deleteUser", function (e) {
            var id = $(this).data("id");
            var token = $(this).data("token");
            swal.fire({
                title: "Delete User",
                text: "Are You Sure Want to Delete this Package?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "DELETE!",
                cancelButtonText: "CANCEL",
                reverseButtons: !0
            }).then(function (e) {

                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                    $.ajax(
                        {
                            url: "packages/delete/" + id,
                            type: 'GET',
                            data: {
                                "id": id,
                                "_token": token,
                            },
                            success: function (response) {
                                console.log(response);
                                if (response.success === true) {
                                    swal.fire("Package Deleted Successfully!", response.message, "success");
                                    // location.reload();
                                } else {
                                    swal.fire("Package not delete!!", response.message, "error");
                                    // location.reload();
                                }
                            }
                        });
                } else {
                    e.dismiss;
                }

            })

        });
    });


    $(function () {

        var table = $('.data-table').DataTable({

            processing: true,
            serverSide: true,
            ajax: "{{ url('admin/users') }}",
            columns: [
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'role', name: 'role'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'status', name: 'status'},
                {data: 'created_at', name: 'created_at'},

            ]
        });

    });


</script>

<!-- endinject -->
<!-- custom js for this page -->
{{--<script src="../../../assets/js/data-table.js"></script>--}}

<!-- end custom js for this page -->

</html>

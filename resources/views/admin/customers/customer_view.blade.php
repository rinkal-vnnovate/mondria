@extends('includes.nav')
@include('includes.header')

<body>
<div class="main-wrapper">
    <div class="page-wrapper">
        <div class="page-content">
            <nav class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('/admin/customer_info'); ?>">Customers</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Customer Details</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card" style="padding: 11px;">
                        <div class="card-body" >
                            <h6 class="card-title" style="margin-bottom: unset;">Customer Details </h6>
                            <div id="accordion" class="accordion" role="tablist">
                                <div class="card" style="width: 987px;">
                                   <div class="card-body table-responsive">
                                       <div class="form-row">
                                           <div class="col-md-4 mb-3">
                                               <div class="form-group">
                                                    <div class="row">
                                                       <strong>Customer Name </strong>
                                                        <p class="px-2">{{ $data->customer}}</p>
                                                    </div>
                                               </div>
                                           </div>
                                           <div class="col-md-4 mb-3">
                                               <div class="form-group">
                                                   <div class="row">
                                                       <strong>Email </strong>
                                                       <p class="px-2">{{ $data->email }}</p>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-md-4 mb-3">
                                               <div class="form-group">
                                                   <div class="row">
                                                       <strong>Contact No. </strong>
                                                       <p class="px-2">{{ $data->contact }}</p>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>

                                            <div class="form-row">
                                                <div class="col-md-4 mb-3">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <strong>Address </strong>
                                                            <p class="px-2">A,Skyline Heights,Iscon.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                           </div>
                                </div>
                            </div>
                        </div>
{{--                            Flat Details--}}
                            <h6 class="card-title" style="margin-bottom: unset;">Package Details :-</h6>
                            <div id="accordion" class="accordion" role="tablist">
                                <div class="card" style="width: 987px;" >
                                    <div class="card-body table-responsive">
                                        <div class="form-row">

                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Scheme Name </strong>
                                                        <p class="px-2">Skyline Heights</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Package Name </strong>
                                                        <p class="px-2">LUMINOUS</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Sub-Package Name </strong>
                                                        <p class="px-2">STANDARD LIGHT PACKAGE</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Layout Plan </strong>
                                                        <p class="px-2">{{ $data->plan }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Area (Sq/ft) </strong>
                                                        <p class="px-2">{{$data->sq_feet }} Sq / Ft</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Flat No. </strong>
                                                        <p class="px-2" style="word-wrap: normal;">{{ $data->house_no }}</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-row">

                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Floor </strong>
                                                        <p class="px-2">{{$data->floor }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Block </strong>
                                                        <p class="px-2">{{ $data['block']->name }}</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>


                                    </div>
                                </div>
{{--                             Flat Details--}}
                            <h6 class="card-title" style="margin-bottom: unset;">Living Room Details </h6>
                            <div id="accordion" class="accordion" role="tablist">
                                <div class="card" style="width: 987px;">
                                    <div class="card-body table-responsive">
                                        <div class="form-row">

                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Living Room Size </strong>
                                                        <p class="px-2">14'-2" × 12'-1"</p>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>


                                </div>
                            </div>
                            <h6 class="card-title" style="margin-bottom: unset;">Bed Room Details </h6>
                            <div id="accordion" class="accordion" role="tablist">
                                <div class="card" style="width: 987px;">
                                    <div class="card-body table-responsive">
                                        <div class="form-row">

                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Layout Plan </strong>
                                                        <p class="px-2">{{ $data->plan }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Total Bedroom </strong>
                                                        <p class="px-2">2</p>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Bedroom Size </strong>
                                                        <ul>
                                                        <li>10'-2" × 11'-1"</li>
                                                        <li>10'-0" × 12'-8"</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>


                                </div>
                            </div>
                            <h6 class="card-title" style="margin-bottom: unset;">Wash Room Details </h6>
                            <div id="accordion" class="accordion" role="tablist">
                                <div class="card" style="width: 987px;">
                                    <div class="card-body table-responsive">
                                        <div class="form-row">
                                            <div class="col-md-4 mb-3">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <strong>Wash Room Size </strong>
                                                        <p class="px-2">4'-2" × 8'-1"</p>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>


                                </div>
                            </div>

                            </div>
                    </div>
                </div>
            </div>


        </div>

        <!-- partial:../../partials/_footer.html -->
        <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
            <p class="text-muted text-center text-md-left">Copyright © 2022 <a href="https://monderia.iqlanceserver.com/"
                                                                               target="_blank">Monderia</a>. All rights
                reserved</p>
            {{--            <p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Handcrafted With <i--}}
            {{--                    class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p>--}}
        </footer>
        <!-- partial -->

    </div>
</div>

<!-- core:js -->
<script src="../../../assets/vendors/core/core.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<script src="../../../assets/vendors/jquery-validation/jquery.validate.min.js"></script>
<script src="../../../assets/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="../../../assets/vendors/inputmask/jquery.inputmask.min.js"></script>
<script src="../../../assets/vendors/select2/select2.min.js"></script>
<script src="../../../assets/vendors/typeahead.js/typeahead.bundle.min.js"></script>
<script src="../../../assets/vendors/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="../../../assets/vendors/dropzone/dropzone.min.js"></script>
<script src="../../../assets/vendors/dropify/dist/dropify.min.js"></script>
<script src="../../../assets/vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="../../../assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="../../../assets/vendors/moment/moment.min.js"></script>
<script src="../../../assets/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.js"></script>
<!-- end plugin js for this page -->
<!-- inject:js -->
<script src="../../../assets/vendors/feather-icons/feather.min.js"></script>
<script src="../../../assets/js/template.js"></script>
<!-- endinject -->
<!-- custom js for this page -->
<script src="../../../assets/js/form-validation.js"></script>
<script src="../../../assets/js/bootstrap-maxlength.js"></script>
<script src="../../../assets/js/inputmask.js"></script>
<script src="../../../assets/js/select2.js"></script>
<script src="../../../assets/js/typeahead.js"></script>
<script src="../../../assets/js/tags-input.js"></script>
<script src="../../../assets/js/dropzone.js"></script>
<script src="../../../assets/js/dropify.js"></script>
<script src="../../../assets/js/bootstrap-colorpicker.js"></script>
<script src="../../../assets/js/datepicker.js"></script>
<script src="../../../assets/js/timepicker.js"></script>
<!-- end custom js for this page -->
</body>
</html>

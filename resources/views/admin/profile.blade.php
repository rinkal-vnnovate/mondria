@extends('includes.nav')
@include('includes.header')
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Monderia</title>
    <!-- core:css -->
    <link rel="stylesheet" href="../../../assets/vendors/core/core.css">
    <link rel="stylesheet" href="../../../assets/vendors/select2/select2.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/jquery-tags-input/jquery.tagsinput.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/font-awesome/css/font-awesome.min.css">

    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- end plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="../../../assets/fonts/feather-font/css/iconfont.css">
    <link rel="stylesheet" href="../../../assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="../../../assets/css/demo_1/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="../../../assets/images/favicon.png"/>
</head>
<body>
<div class="main-wrapper">

    <!-- partial:../../partials/_sidebar.html -->

    <nav class="settings-sidebar">
        <div class="sidebar-body">
            <a href="#" class="settings-sidebar-toggler">
                <i data-feather="settings"></i>
            </a>
            <h6 class="text-muted">Sidebar:</h6>
            <div class="form-group border-bottom">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="sidebarThemeSettings" id="sidebarLight"
                               value="sidebar-light" checked>
                        Light
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="sidebarThemeSettings" id="sidebarDark"
                               value="sidebar-dark">
                        Dark
                    </label>
                </div>
            </div>
            <div class="theme-wrapper">
                <h6 class="text-muted mb-2">Light Theme:</h6>
                <a class="theme-item active" href="../../../demo_1/dashboard-one.html">
                    <img src="../../../assets/images/screenshots/light.jpg" alt="light theme">
                </a>
                <h6 class="text-muted mb-2">Dark Theme:</h6>
                <a class="theme-item" href="../../../demo_2/dashboard-one.html">
                    <img src="../../../assets/images/screenshots/dark.jpg" alt="light theme">
                </a>
            </div>
        </div>
    </nav>
    <!-- partial -->
    <div class="page-wrapper">

        <!-- partial:../../partials/_navbar.html -->
        <!-- partial -->

        <div class="container-fluid">
            <br><br><br><br>
            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="ik ik-x"></i>
                    </button>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <?php
                                if($user['avatar'] == ''){ ?>
                                <img src="<?php echo URL::to('/uploads/avatar.jpg'); ?>" class="rounded-circle"
                                     width="150"/>
                                <?php
                                }else{
                                ?>
                                <img src="<?php echo URL::to('/uploads'); ?>/{{ $user->avatar }}" class="rounded-circle"
                                     width="150"/>
                                <?php
                                }
                                ?>
                                <h4 class="card-title mt-10"></h4>
                                <p class="card-subtitle"></p>
                            </div>
                        </div>
                        <hr class="mb-0">
                    </div>
                </div>
                <div class="col-lg-8 col-md-7">
                    <div class="card">
                        <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#last-month"
                                   role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                                   role="tab" aria-controls="pills-setting" aria-selected="false">Reset Password</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="last-month" role="tabpanel"
                                 aria-labelledby="pills-profile-tab">
                                <div class="card-body">
                                    <form id="profileUpdate" class="form-horizontal" enctype="multipart/form-data"
                                          action=" <?php echo URL::to('admin/updateProfile'); ?> " method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $user->id }}">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="example-name">{{ __('Name')}}</label>
                                                <input type="text" value="{{$user->name}}"
                                                       class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                       name="name" placeholder="Name">
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="example-email">{{ __('Email')}}</label>
                                                <input type="email" value="{{$user->email}}"
                                                       class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                       name="email" placeholder="Email">
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                         </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="form-group col-md-6">
                                                <label for="Avatar">Profile</label>
                                                <input class="form-control" name="avatar" id="avatar"  type="file"  onchange="PreviewImage();" accept=".png, .jpg, .jpeg, .gif">
                                                <?php
                                                if($user['avatar'] == ''){ ?>
                                                <img src="<?php echo URL::to('/uploads/avatar.jpg'); ?>"
                                                     class="rounded-circle" width="50" height="50"/>
                                                <?php
                                                }else{
                                                ?>
                                                <img src="<?php echo URL::to('/uploads'); ?>/{{ $user->avatar }}"
                                                     class="rounded-circle" id="uploadPreview" width="50" height="50"/>
                                                <?php
                                                }
                                                ?>

                                            </div>
                                        </div>


                                        <button class="btn btn-success" type="submit">Update Profile</button>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane fade show" id="previous-month" role="tabpanel"
                                 aria-labelledby="pills-setting-tab">
                                <div class="card-body">
                                    <form class="form-horizontal" id="passwordUpdate" role="form" method="post"
                                          action="<?php echo URL::to('admin/updatePassword'); ?>">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $user->id }}">
                                        <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="example-password">Enter New Password</label>
                                            <input type="password" id="password" data-toggle="password"
                                                   data-message="keep secret"
                                                   data-placement="before"
                                                   class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                   name="password" placeholder="Password" value="">
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="confirm_password">Confirm password</label>
                                            <input id="confirm_password" class="form-control"
                                                   placeholder="Confirm password" name="confirm_password"
                                                   type="password">

                                        </div>
                                        </div>
                                        <button class="btn btn-success" type="submit">Update Password</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- partial:../../partials/_footer.html -->
        <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
            <p class="text-muted text-center text-md-left">Copyright © 2022 <a
                    href="https://monderia.iqlanceserver.com/"
                    target="_blank">Monderia</a>. All rights
                reserved</p>
            {{--            <p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Handcrafted With <i--}}
            {{--                    class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p>--}}
        </footer>
        <!-- partial -->

    </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        function PreviewImage() {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("avatar").files[0]);

            oFReader.onload = function (oFREvent) {
                document.getElementById("uploadPreview").src = oFREvent.target.result;
            };
        };
        $('#profileUpdate').validate({ // initialize the plugin
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },

            },
            messages: {
                name: {
                    required: "Please enter a name",
                },
                email: {
                    required: "Please enter a email address",
                    email: "Please enter a valid email address"
                },

            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    });
    $(document).ready(function () {
        $('#passwordUpdate').validate({ // initialize the plugin
            rules: {
                password: {
                    required: true
                },
                confirm_password: {
                    required: true,
                    equalTo: "#password"
                },
            },
            messages: {
                password: {
                    required: "Please enter a Password",
                },
                confirm_password: {
                    required: "Please enter a Confirm Password",
                    equalTo: "PassWord & Confirm Password must be same!!",

                },
            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    });
</script>

<!-- core:js -->
<script src="../../../assets/vendors/core/core.js"></script>
<script src="../../../assets/vendors/jquery-validation/jquery.validate.min.js"></script>
<script src="../../../assets/js/form-validation.js"></script>
<script src="../../../assets/js/select2.js"></script>

<!-- endinject -->
<!-- plugin js for this page -->
<!-- end plugin js for this page -->
<!-- inject:js -->
<script src="../../../assets/vendors/feather-icons/feather.min.js"></script>
<script src="../../../assets/js/template.js"></script>
<!-- endinject -->
<!-- custom js for this page -->
<!-- end custom js for this page -->
</body>
</html>

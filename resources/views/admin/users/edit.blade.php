@extends('includes.nav')
@include('includes.header')
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Monderia</title>
    <!-- core:css -->
    <link rel="stylesheet" href="../../../assets/vendors/core/core.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="../../../assets/vendors/select2/select2.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/jquery-tags-input/jquery.tagsinput.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/dropzone/dropzone.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/dropify/dist/dropify.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../assets/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.min.css">
    <!-- end plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="../../../assets/fonts/feather-font/css/iconfont.css">
    <link rel="stylesheet" href="../../../assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="../../../assets/css/demo_1/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="../../../assets/images/favicon.png" />
</head>
<body>
<div class="main-wrapper">

    <!-- partial:../../partials/_sidebar.html -->
    <nav class="settings-sidebar">
        <div class="sidebar-body">
            <a href="#" class="settings-sidebar-toggler">
                <i data-feather="settings"></i>
            </a>
            <h6 class="text-muted">Sidebar:</h6>
            <div class="form-group border-bottom">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="sidebarThemeSettings" id="sidebarLight" value="sidebar-light" checked>
                        Light
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="sidebarThemeSettings" id="sidebarDark" value="sidebar-dark">
                        Dark
                    </label>
                </div>
            </div>
            <div class="theme-wrapper">
                <h6 class="text-muted mb-2">Light Theme:</h6>
                <a class="theme-item active" href="../../../demo_1/dashboard-one.html">
                    <img src="../../../assets/images/screenshots/light.jpg" alt="light theme">
                </a>
                <h6 class="text-muted mb-2">Dark Theme:</h6>
                <a class="theme-item" href="../../../demo_2/dashboard-one.html">
                    <img src="../../../assets/images/screenshots/dark.jpg" alt="light theme">
                </a>
            </div>
        </div>
    </nav>
    <!-- partial -->

    <div class="page-wrapper">

        <!-- partial:../../partials/_navbar.html -->

        <!-- partial -->

        <div class="page-content">

            <nav class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('/admin/users'); ?>">Users</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit User</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Edit User</h4>
                            <form id="editUserForm" method="post" action=" {{ route('admin.users.update') }} ">
                                @csrf
                                 <div class="row">
                                     <div class="form-group col-md-4">
                                         <input type="hidden" name="id" value="{{ $data->id }}">
                                         <label>Role</label>
                                         <select class="js-example-basic-single w-100" name="role">
{{--                                             <option {{ old('role',$data->role)=="ADMIN"? 'selected':''}} value="ADMIN">ADMIN</option>--}}
                                             <option {{ old('role',$data->role)=="AGENT"? 'selected':''}} value="AGENT">AGENT</option>
                                         </select>
                                     </div>
                                    <div class="form-group col-md-4">
                                        <label for="name">Name</label>
                                        <input id="name" value="{{ $data->name }}" class="form-control" name="name" type="text">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="email">Email</label>
                                        <input id="email" value="{{ $data->email }}" class="form-control" name="email" type="email">
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="password">Password</label>
                                        <input id="password" class="form-control" {{ $data->password }} name="password" type="password">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="confirm_password">Confirm password</label>
                                        <input id="confirm_password" class="form-control"  {{ $data->password }} name="confirm_password" type="password">
                                    </div>
                                </div>
                                <button class="btn btn-primary" type="submit" >Update</button>
                                <a class="btn btn-danger" href="<?php echo URL::to('/admin/users'); ?>" >Cancel</a>

                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <!-- partial:../../partials/_footer.html -->
        <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
            <p class="text-muted text-center text-md-left">Copyright © 2022 <a href="https://monderia.iqlanceserver.com/"
                                                                               target="_blank">Monderia</a>. All rights
                reserved</p>
            {{--            <p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Handcrafted With <i--}}
            {{--                    class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p>--}}
        </footer>
        <!-- partial -->

    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('#editUserForm').validate({ // initialize the plugin
            rules: {
                role: {
                    required: true
                },
                name: {
                    required: true,
                    minlength: 3
                },
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                },
            },
            messages: {
                role: {
                    required: "Please Select Role",
                },
                name: {
                    required: "Please enter a Name",
                    minlength: "Name must consist of at least 3 characters"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a Confirm password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                email: {
                    required: "Please enter a email address",
                    email : "Please enter a valid email address",
                },
            },
            errorPlacement: function(label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    });

</script>
<!-- core:js -->
<script src="../../../assets/vendors/core/core.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<script src="../../../assets/vendors/jquery-validation/jquery.validate.min.js"></script>
<script src="../../../assets/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="../../../assets/vendors/inputmask/jquery.inputmask.min.js"></script>
<script src="../../../assets/vendors/select2/select2.min.js"></script>
<script src="../../../assets/vendors/typeahead.js/typeahead.bundle.min.js"></script>
<script src="../../../assets/vendors/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="../../../assets/vendors/dropzone/dropzone.min.js"></script>
<script src="../../../assets/vendors/dropify/dist/dropify.min.js"></script>
<script src="../../../assets/vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="../../../assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="../../../assets/vendors/moment/moment.min.js"></script>
<script src="../../../assets/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.js"></script>
<!-- end plugin js for this page -->
<!-- inject:js -->
<script src="../../../assets/vendors/feather-icons/feather.min.js"></script>
<script src="../../../assets/js/template.js"></script>
<!-- endinject -->
<!-- custom js for this page -->
<script src="../../../assets/js/form-validation.js"></script>
<script src="../../../assets/js/bootstrap-maxlength.js"></script>
<script src="../../../assets/js/inputmask.js"></script>
<script src="../../../assets/js/select2.js"></script>
<script src="../../../assets/js/typeahead.js"></script>
<script src="../../../assets/js/tags-input.js"></script>
<script src="../../../assets/js/dropzone.js"></script>
<script src="../../../assets/js/dropify.js"></script>
<script src="../../../assets/js/bootstrap-colorpicker.js"></script>
<script src="../../../assets/js/datepicker.js"></script>
<script src="../../../assets/js/timepicker.js"></script>
<!-- end custom js for this page -->
</body>
</html>

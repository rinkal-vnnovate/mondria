<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Monderia</title>
    <!-- core:css -->
    <link rel="stylesheet" href="../../../assets/vendors/core/core.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- end plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="../../../assets/fonts/feather-font/css/iconfont.css">
    <link rel="stylesheet" href="../../../assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="../../../assets/css/demo_1/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="../../../assets/images/favicon.png"/>

</head>
<body class="sidebar-dark">
<div class="main-wrapper">
    <div class="page-wrapper full-page"
         style="background-image: url('<?php echo URL::to('/uploads/SnapEdit.jpg'); ?>');background-repeat: no-repeat;background-size: cover;">
        <div class="page-content d-flex align-items-center justify-content-center">

            <div class="row w-100 mx-0 auth-page" style="opacity: 0.9;">
                <div class="col-md-8 col-xl-6 mx-auto">
                    <div class="card">
                        @if (\Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="ik ik-x"></i>
                                </button>
                            </div>
                        @elseif(\Session::has('error'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="ik ik-x"></i>
                                </button>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12 pl-md-0">
                                <div class="auth-form-wrapper px-4 py-5">
                                    <div class="col-md-12">
                                    <h5 class="text-muted font-weight-normal mb-4" style="text-align: center;">Reset Password</h5>
                                    </div>
                                    <form id="forgetPasswordForm" method="post" action="{{ route('ForgetPasswordPost') }}">
                                        @csrf
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="email_address">E-Mail Address</label>
                                                <input type="text" id="email" class="form-control" name="email" placeholder="Email" required autofocus>
                                                @if ($errors->has('email'))
                                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <div class="mt-3">
                                                <button type="submit"
                                                        class="btn btn-primary mr-2 mb-2 mb-md-0 text-white"> Send Reset Password Link
                                                </button>
                                            </div>
                                        </div>
                                        {{--                                        <a href="{{ url('/register') }}" class="d-block mt-3 text-muted">Not a user? Sign up</a>--}}
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>

        $(document).ready(function () {
            $.validator.addMethod("customemail",
                function (value, element) {
                    return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
                },
                "Sorry, I've enabled very strict email validation"
            );
            $('#forgetPasswordForm').validate({ // initialize the plugin
                rules: {
                    email: {
                        required: true,
                        email: true,
                        customemail: true,
                    },

                },
                messages: {
                    email: {
                        required: "Please enter a Email",
                        email: "Please enter a valid email address",
                        IsEmail: "Please enter a valid email address"
                    },
                },
                errorPlacement: function(label, element) {
                    label.addClass('mt-2 text-danger');
                    label.insertAfter(element);
                },
                highlight: function(element, errorClass) {
                    $(element).parent().addClass('has-danger')
                    $(element).addClass('form-control-danger')
                }
            });
        });
        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }


</script>


<!-- core:js -->
<script src="../../../assets/vendors/core/core.js"></script>
<script src="../../../assets/vendors/jquery-validation/jquery.validate.min.js"></script>
<script src="../../../assets/js/form-validation.js"></script>
<script src="../../../assets/js/select2.js"></script>

<!-- endinject -->
<!-- plugin js for this page -->
<!-- end plugin js for this page -->
<!-- inject:js -->
<script src="../../../assets/vendors/feather-icons/feather.min.js"></script>
<script src="../../../assets/js/template.js"></script>
</body>
</html>

